#ifndef SHM_QUEUE_H
#define SHM_QUEUE_H

#include <iostream>
#include <chrono>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define SIZE 1000
//#define SHM_HUGE_SHIFT  26
//#define SHM_HUGE_MASK   0x3f
///* Flags are encoded in bits (SHM_HUGE_MASK << SHM_HUGE_SHIFT) */

//#define SHM_HUGE_2MB    (21 << SHM_HUGE_SHIFT)   /* 2 MB huge pages */
//#define SHM_HUGE_1GB    (30 << SHM_HUGE_SHIFT)   /* 1 GB huge pages */


struct packet
{
    std::chrono::high_resolution_clock::time_point tp;
    char str[256];
    int value;
};

int map_thread_to_cpu(int core_id)
{
   int num_cores = sysconf(_SC_NPROCESSORS_ONLN);
   if (core_id < 0 || core_id >= num_cores) {
        return -1;
   }
   cpu_set_t cpuset;
   CPU_ZERO(&cpuset);
   CPU_SET(core_id, &cpuset);
   pthread_t current_thread = pthread_self();

   return pthread_setaffinity_np(current_thread, sizeof(cpu_set_t), &cpuset);
}


template <class X>
class shm_queue
{
    X *buffer; 		
    int front;
    int *back;   	

public:

    shm_queue(unsigned char id, int size = SIZE);		// constructor

    void enqueue(X x);
    bool dequeue(X &output);
    void reset();

private:

    int capacity;
};

template <class X>
shm_queue<X>::shm_queue(unsigned char id, int size) //Constructor - don't use consecutive id's for different queues
{
    capacity = size;

    key_t bufferKey = ftok("/tmp",id);
    key_t backKey = ftok("/tmp",id+1);
//    int pageSize = 1024;
//    int error;
//    for( int i = 0; i < 10; i++ )
//    {
//        int x = shmget(ftok(std::filesystem::current_path().string().c_str(),id+2+i), pageSize, 0666|IPC_CREAT);
//        printf("Shared memory identifier is %d \n",x);
//        if( x == -1 )
//        {
//            error = errno;
//            break;
//        }

//        pageSize = pageSize * 2;
//    }
//    int dummyBuffer = shmget(ftok(std::filesystem::current_path().string().c_str(),id+20), pageSize, 0666|IPC_CREAT| SHM_HUGETLB);
//    int dummyBuffer2 = shmget(ftok(std::filesystem::current_path().string().c_str(),id+21), pageSize * 2, 0666|IPC_CREAT| SHM_HUGETLB);
//    if ((dummyBuffer < 0) || (dummyBuffer2 < 0))
//    {
//        std::cout << "failed to create" << std::endl;
//    }
    int shmidBuffer = shmget(bufferKey, size * sizeof(X), 0666|IPC_CREAT);
    std::string errorString = strerror(errno);
    int shmidBack = shmget(backKey, sizeof (int), 0666|IPC_CREAT);
    if ((shmidBuffer < 0) || (shmidBack < 0))
    {
        std::cout << "shm_queue: error!" << " ,shmidBuffer: " << errorString << " ,shmidBack: " << shmidBack << std::endl;
        return;
    }

    buffer = (X*)shmat(shmidBuffer, (void*)0, 0); //attach buf to shared memory

    front = 0;
    back = (int*)shmat(shmidBack, (void*)0,0);
    *back = 0;
}

template <class X>
void shm_queue<X>::enqueue(X item)
{
    (*back) = ((*back) + 1) % capacity;
    buffer[*back] = item;
}

template <class X>
bool shm_queue<X>::dequeue(X& output)
{
    if (front != *back)
    {
        front = (front + 1) % capacity;
        output = buffer[front];
        return(true);
    }

    return(false);
}

template <class X>
void shm_queue<X>::reset() //sets the start from last element
{
    front = *back;
}

/******************************************Some semaphore experiment******************************/
#include <semaphore.h>
#include <assert.h>
class Semaphore
{
private:
    sem_t* m_sema;

    Semaphore(const Semaphore& other) = delete;
    Semaphore& operator=(const Semaphore& other) = delete;

public:
    Semaphore(int initialCount = 0)
    {
        assert(initialCount >= 0);


        int shmid = shmget(3000, sizeof(sem_t), 0666|IPC_CREAT);
        m_sema = (sem_t*)shmat(shmid, (void*)0, 0); //attach buf to shared memory

        std::cout << sem_init(m_sema, 1, initialCount) << std::endl;
    }

    ~Semaphore()
    {
        sem_destroy(m_sema);
    }

    void wait()
    {
        // http://stackoverflow.com/questions/2013181/gdb-causes-sem-wait-to-fail-with-eintr-error
        int rc;
        do
        {
            rc = sem_wait(m_sema);
        }
        while (rc == -1 && errno == EINTR);
    }

    bool try_wait()
    {
        int rc;
        do {
            rc = sem_trywait(m_sema);
        } while (rc == -1 && errno == EINTR);
        return !(rc == -1 && errno == EAGAIN);
    }

    bool timed_wait(std::uint64_t usecs)
    {
        struct timespec ts;
        const int usecs_in_1_sec = 1000000;
        const int nsecs_in_1_sec = 1000000000;
        clock_gettime(CLOCK_REALTIME, &ts);
        ts.tv_sec += usecs / usecs_in_1_sec;
        ts.tv_nsec += (usecs % usecs_in_1_sec) * 1000;
        // sem_timedwait bombs if you have more than 1e9 in tv_nsec
        // so we have to clean things up before passing it in
        if (ts.tv_nsec >= nsecs_in_1_sec) {
            ts.tv_nsec -= nsecs_in_1_sec;
            ++ts.tv_sec;
        }

        int rc;
        do {
            rc = sem_timedwait(m_sema, &ts);
        } while (rc == -1 && errno == EINTR);
        return !(rc == -1 && errno == ETIMEDOUT);
    }

    void signal()
    {
        sem_post(m_sema);
    }

    void signal(int count)
    {
        while (count-- > 0)
        {
            sem_post(m_sema);
        }
    }
};


//---------------------------------------------------------
// LightweightSemaphore
//---------------------------------------------------------

#include <atomic>
#include <memory>

class LightweightSemaphore
{
public:
    typedef std::make_signed<std::size_t>::type ssize_t;

private:
    std::atomic<ssize_t> m_count;
    Semaphore m_sema;

    bool waitWithPartialSpinning(std::int64_t timeout_usecs = -1)
    {
        ssize_t oldCount;
        // Is there a better way to set the initial spin count?
        // If we lower it to 1000, testBenaphore becomes 15x slower on my Core i7-5930K Windows PC,
        // as threads start hitting the kernel semaphore.
        int spin = 10000;
        while (--spin >= 0)
        {
            oldCount = m_count.load(std::memory_order_relaxed);
            if ((oldCount > 0) && m_count.compare_exchange_strong(oldCount, oldCount - 1, std::memory_order_acquire, std::memory_order_relaxed))
                return true;
            std::atomic_signal_fence(std::memory_order_acquire);	 // Prevent the compiler from collapsing the loop.
        }
        oldCount = m_count.fetch_sub(1, std::memory_order_acquire);
        if (oldCount > 0)
            return true;
        if (timeout_usecs < 0)
        {
            m_sema.wait();
            return true;
        }
        if (m_sema.timed_wait((std::uint64_t)timeout_usecs))
            return true;
        // At this point, we've timed out waiting for the semaphore, but the
        // count is still decremented indicating we may still be waiting on
        // it. So we have to re-adjust the count, but only if the semaphore
        // wasn't signaled enough times for us too since then. If it was, we
        // need to release the semaphore too.
        while (true)
        {
            oldCount = m_count.load(std::memory_order_acquire);
            if (oldCount >= 0 && m_sema.try_wait())
                return true;
            if (oldCount < 0 && m_count.compare_exchange_strong(oldCount, oldCount + 1, std::memory_order_relaxed, std::memory_order_relaxed))
                return false;
        }
    }

    ssize_t waitManyWithPartialSpinning(ssize_t max, std::int64_t timeout_usecs = -1)
    {
        assert(max > 0);
        ssize_t oldCount;
        int spin = 10000;
        while (--spin >= 0)
        {
            oldCount = m_count.load(std::memory_order_relaxed);
            if (oldCount > 0)
            {
                ssize_t newCount = oldCount > max ? oldCount - max : 0;
                if (m_count.compare_exchange_strong(oldCount, newCount, std::memory_order_acquire, std::memory_order_relaxed))
                    return oldCount - newCount;
            }
            std::atomic_signal_fence(std::memory_order_acquire);
        }
        oldCount = m_count.fetch_sub(1, std::memory_order_acquire);
        if (oldCount <= 0)
        {
            if (timeout_usecs < 0)
                m_sema.wait();
            else if (!m_sema.timed_wait((std::uint64_t)timeout_usecs))
            {
                while (true)
                {
                    oldCount = m_count.load(std::memory_order_acquire);
                    if (oldCount >= 0 && m_sema.try_wait())
                        break;
                    if (oldCount < 0 && m_count.compare_exchange_strong(oldCount, oldCount + 1, std::memory_order_relaxed, std::memory_order_relaxed))
                        return 0;
                }
            }
        }
        if (max > 1)
            return 1 + tryWaitMany(max - 1);
        return 1;
    }

public:
    LightweightSemaphore(ssize_t initialCount = 0) : m_count(initialCount)
    {
        assert(initialCount >= 0);
    }

    bool tryWait()
    {
        ssize_t oldCount = m_count.load(std::memory_order_relaxed);
        while (oldCount > 0)
        {
            if (m_count.compare_exchange_weak(oldCount, oldCount - 1, std::memory_order_acquire, std::memory_order_relaxed))
                return true;
        }
        return false;
    }

    void wait()
    {
        if (!tryWait())
            waitWithPartialSpinning();
    }

    bool wait(std::int64_t timeout_usecs)
    {
        return tryWait() || waitWithPartialSpinning(timeout_usecs);
    }

    // Acquires between 0 and (greedily) max, inclusive
    ssize_t tryWaitMany(ssize_t max)
    {
        assert(max >= 0);
        ssize_t oldCount = m_count.load(std::memory_order_relaxed);
        while (oldCount > 0)
        {
            ssize_t newCount = oldCount > max ? oldCount - max : 0;
            if (m_count.compare_exchange_weak(oldCount, newCount, std::memory_order_acquire, std::memory_order_relaxed))
                return oldCount - newCount;
        }
        return 0;
    }

    // Acquires at least one, and (greedily) at most max
    ssize_t waitMany(ssize_t max, std::int64_t timeout_usecs)
    {
        assert(max >= 0);
        ssize_t result = tryWaitMany(max);
        if (result == 0 && max > 0)
            result = waitManyWithPartialSpinning(max, timeout_usecs);
        return result;
    }

    ssize_t waitMany(ssize_t max)
    {
        ssize_t result = waitMany(max, -1);
        assert(result > 0);
        return result;
    }

    void signal(ssize_t count = 1)
    {
        assert(count >= 0);
        ssize_t oldCount = m_count.fetch_add(count, std::memory_order_release);
        ssize_t toRelease = -oldCount < count ? -oldCount : count;
        if (toRelease > 0)
        {
            m_sema.signal((int)toRelease);
        }
    }

    ssize_t availableApprox() const
    {
        ssize_t count = m_count.load(std::memory_order_relaxed);
        return count > 0 ? count : 0;
    }
};

/******************************************Dimitri Vyukov queue************* */

#include <type_traits>

template <typename T>
class MpscQueue
{

public:
    struct Node {
        std::atomic<Node*> next;
        T value;
    };

    MpscQueue() : stub(new Node()), head(stub.get()), tail(stub.get()) {
        stub->next.store(nullptr);
    }

    void push(const T& input) {
        Node* node = new Node();
        node->value = input;
        node->next.store(nullptr, std::memory_order_relaxed);
        Node* prev = tail.exchange(node, std::memory_order_acq_rel);
        prev->next.store(node, std::memory_order_release);
    }

    /*Node* pop() {
        Node* head_copy = head.load(std::memory_order_relaxed);
        Node* next = head_copy->next.load(std::memory_order_acquire);

        if (next != nullptr) {
            head.store(next, std::memory_order_relaxed);
            head_copy->value = next->value;
            return head_copy;
        }
        return nullptr;
    }*/

    bool pop(T* value) {
            std::cout << sizeof(std::atomic<Node*>) << std::endl;
            Node* head_copy = head.load(std::memory_order_relaxed);
            Node* next = head_copy->next.load(std::memory_order_acquire);

            if (next != nullptr) {
                *value = next->value;
                head.store(next, std::memory_order_relaxed);
                return(true);
            }

            return(false);
        }

private:
      std::unique_ptr<Node> stub;
      std::atomic<Node*> head;
      std::atomic<Node*> tail;
};


#include <atomic>
#include <cassert>
#include <type_traits>

/**
 * Multiple Producer Single Consumer Lockless Q
 */
template <typename T>
class mpsc_queue_t {
 public:
  struct buffer_node_t {
    T data;
    std::atomic<buffer_node_t*> next;
  };

  mpsc_queue_t() {
      /*int shmidBNA = shmget(id, sizeof(buffer_node_aligned_t), 0666|IPC_CREAT);
      int shmidBNT = shmget(id + 1, sizeof(int), 0666|IPC_CREAT);

      if ((shmidBNA < 0) || (shmidBNT < 0))
      {
          std::cout << "shm_queue: error!" << std::endl;
          return;
      }

      buffer_node_aligned_t* al_st = (buffer_node_aligned_t*)shmat(shmidBNA, (void*)0, 0); //attach buf to shared memory */

    buffer_node_aligned_t* al_st = new buffer_node_aligned_t;
    buffer_node_t* node = new (al_st) buffer_node_t();
    _head.store(node);
    _tail.store(node);

    node->next.store(nullptr, std::memory_order_relaxed);
  }

  ~mpsc_queue_t() {
    T output;
    while (this->dequeue(&output)) {
    }
    buffer_node_t* front = _head.load(std::memory_order_relaxed);
    front->~buffer_node_t();

    ::operator delete(front);
  }

  void enqueue(const T& input) {
    buffer_node_aligned_t* al_st = new buffer_node_aligned_t;
    buffer_node_t* node = new (al_st) buffer_node_t();

    node->data = input;
    node->next.store(nullptr, std::memory_order_relaxed);

    buffer_node_t* prev_head = _head.exchange(node, std::memory_order_acq_rel);
    prev_head->next.store(node, std::memory_order_release);
  }

  bool dequeue(T* output) {
    buffer_node_t* tail = _tail.load(std::memory_order_relaxed);
    buffer_node_t* next = tail->next.load(std::memory_order_acquire);

    if (next == nullptr) {
      return false;
    }

    *output = next->data;
    _tail.store(next, std::memory_order_release);

    tail->~buffer_node_t();

    ::operator delete(tail);
    return true;
  }

  // you can only use pop_all if the queue is SPSC
  buffer_node_t* pop_all() {
    // nobody else can move the tail pointer.
    buffer_node_t* tptr = _tail.load(std::memory_order_relaxed);
    buffer_node_t* next =
        tptr->next.exchange(nullptr, std::memory_order_acquire);
    _head.exchange(tptr, std::memory_order_acquire);

    // there is a race condition here
    return next;
  }

 private:
  typedef typename std::aligned_storage<sizeof(buffer_node_t), std::alignment_of<buffer_node_t>::value>::type buffer_node_aligned_t;

  std::atomic<buffer_node_t*> _head;
  std::atomic<buffer_node_t*> _tail;

  mpsc_queue_t(const mpsc_queue_t&) = delete;
  mpsc_queue_t& operator=(const mpsc_queue_t&) = delete;
};
#endif // SHM_QUEUE_H
