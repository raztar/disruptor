#include <iostream>
#include <thread>

#include "../ring_buffer_on_shmem.hpp"
#include "../shared_mem_manager.hpp"
#include "../atomic_print.hpp"
#include "../elapsed_time.hpp"

#include "../NanoLogCpp17.h"

using namespace NanoLog::LogLevels;
using namespace NanoLog;
using namespace std;
using namespace std::chrono;

#ifndef DEBUG_WRITE_
#define DEBUG_WRITE_
#endif

int g_test_index = 0 ;
size_t READ_WRITE_COUNT = 0;

SharedMemRingBuffer g_shared_mem_ring_buffer (YIELDING_WAIT);
SharedMemRingBuffer g_ping_shared_mem_ring_buffer (YIELDING_WAIT);
//SharedMemRingBuffer g_shared_mem_ring_buffer (SLEEPING_WAIT);
//SharedMemRingBuffer g_shared_mem_ring_buffer (BLOCKING_WAIT);

///////////////////////////////////////////////////////////////////////////////
void TestFunc(std::string inputKey, int bufId, int pingBufId)
{
    // Optional: Set the output location for the NanoLog system. By default
    // the log will be output to ./compressedLog

    if(  pingBufId > -1)
    {
        std::string logFileName = "./compressedProducerBuf";
        logFileName.append(to_string(bufId));
        logFileName.append(".log");
        NanoLog::setLogFile(logFileName.c_str());

        // Optional optimization: pre-allocates thread-local data structures
        // needed by NanoLog. This can be invoked once per new
        // thread that will use the NanoLog system.
        NanoLog::preallocate();
    }

    //1. register
    int64_t index_for_customer_use = -1;
    if( pingBufId > -1 )
    {
        if(!g_ping_shared_mem_ring_buffer.RegisterConsumer(0, &index_for_customer_use )) {
            return; //error
        }
    }
    //2. run
    size_t   total_fetched = 0;
    int64_t  read_index = index_for_customer_use ;

    int64_t my_index = -1;
    for(size_t i=1; i <= SharedMemRingBuffer::READ_WRITE_COUNT; i++) {
        OneBufferData my_data;
        my_index = g_shared_mem_ring_buffer.ClaimIndex(0);
        my_data.producer_id = 0;
        if(inputKey.compare("") != 0)
        {
            strcpy( my_data.strVal, inputKey.substr(0,30).c_str() );
        }
        else
        {
            strcpy( my_data.strVal, "Message" );
        }
        my_data.data = i ;
        my_data.tp = std::chrono::high_resolution_clock::now();
        g_shared_mem_ring_buffer.SetData( my_index, &my_data );
        g_shared_mem_ring_buffer.Commit(0, my_index);
        if( pingBufId > -1)
        {
            int64_t returned_index = g_ping_shared_mem_ring_buffer.WaitFor(0, read_index);
            for(int64_t j = read_index; j <= returned_index; j++) {
                //batch job
                OneBufferData* pData = g_ping_shared_mem_ring_buffer.GetData(j);
                int x = duration_cast<nanoseconds>(high_resolution_clock::now() - pData->tp).count();
                NANO_LOG(NOTICE,",Producer %d,%d", pData->data, x);
                g_ping_shared_mem_ring_buffer.CommitRead(0, j );
                total_fetched++;
            }
            read_index = returned_index + 1;
        }
    }
}

int map_thread_to_cpu(int core_id)
{
   int num_cores = sysconf(_SC_NPROCESSORS_ONLN);
   if (core_id < 0 || core_id >= num_cores) {
        return -1;
   }
   cpu_set_t cpuset;
   CPU_ZERO(&cpuset);
   CPU_SET(core_id, &cpuset);
   pthread_t current_thread = pthread_self();

   return pthread_setaffinity_np(current_thread, sizeof(cpu_set_t), &cpuset);
}

///////////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[])
{
    int bufId = 0;
    std::string inputKey = "";
    int pingBufId = -1;
    int cpuIndex = -1;
    for( int i = 0; i < argc-1; i++)
    {
        std::string arg = argv[i];
        if( arg.compare("buf") == 0 )
        {
            std::string bufNo = argv[i+1];
            bufId = std::stoi(bufNo);
            i++;
            continue;
        }
        else if( arg.compare("cpu") == 0 )
        {
            std::string cpuIndexString = argv[i+1];
            cpuIndex = std::stoi(cpuIndexString);
            i++;
            continue;
        }
        else if ( arg.compare("ping") == 0 )
        {
            std::string pingBufIdString = argv[i+1];
            pingBufId = std::stoi(pingBufIdString);
            i++;
            continue;
        }
        else if( arg.compare("key") == 0 )
        {
            inputKey = argv[i+1];
            i++;
            continue;
        }
    }
    if( cpuIndex >= 0)
    {
        map_thread_to_cpu(cpuIndex);
    }
    else
    {
        map_thread_to_cpu(3);
    }
    NanoLog::setLogLevel(NOTICE);
    NANO_LOG(NOTICE,",Message,Time(ns)");

    if(!g_shared_mem_ring_buffer.InitRingBuffer(123456+bufId,SharedMemRingBuffer::MAX_RBUFFER_CAPACITY) ) {
        //Error!
        return 1;
    }
    if( pingBufId > -1 )
    {
        if( !g_ping_shared_mem_ring_buffer.InitRingBuffer(234567+pingBufId,SharedMemRingBuffer::MAX_RBUFFER_CAPACITY) ) {
            //Error!
            return 1;
        }
    }


    ElapsedTime elapsed;
    TestFunc(inputKey, bufId, pingBufId);
    long long nElapsedMicro = elapsed.SetEndTime(MICRO_SEC_RESOLUTION);
    std::cout << "**** producer test " << g_test_index << " / count:"
              << SharedMemRingBuffer::READ_WRITE_COUNT << " -> elapsed : "<< nElapsedMicro << "(micro sec) /"
              << (long long) (10000L*1000000L)/nElapsedMicro <<" TPS\n";
    return 0;
}
