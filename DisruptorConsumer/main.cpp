#include <iostream>
#include <thread>

#include "../ring_buffer_on_shmem.hpp"
#include "../shared_mem_manager.hpp"
#include "../atomic_print.hpp"
#include "../elapsed_time.hpp"
#include "../NanoLogCpp17.h"

using namespace NanoLog::LogLevels;
using namespace NanoLog;
using namespace std;
using namespace std::chrono;

//Wait Strategy
SharedMemRingBuffer g_shared_mem_ring_buffer (YIELDING_WAIT);
SharedMemRingBuffer g_ping_shared_mem_ring_buffer (YIELDING_WAIT);
//SharedMemRingBuffer g_shared_mem_ring_buffer (SLEEPING_WAIT);
//SharedMemRingBuffer g_shared_mem_ring_buffer (BLOCKING_WAIT);

ElapsedTime g_elapsed;

#ifndef DEBUG_READ
#define DEBUG_READ
#endif

#ifndef DEBUG_PRINTF
#define DEBUG_PRINTF
#endif

size_t g_test_index = 0;

///////////////////////////////////////////////////////////////////////////////
void TestFunc(int customer_id, int bufId, int pingId)
{
        //    // Nano log setup
        //    // Optional: Set the output location for the NanoLog system. By default
        //    // the log will be output to ./compressedLog
        //

        std::string logFileName = "./compressedConsumerBuf";
        logFileName.append(to_string(bufId));
        logFileName.append(".log");
        NanoLog::setLogFile(logFileName.c_str());

        //    // Optional optimization: pre-allocates thread-local data structures
        //    // needed by NanoLog. This can be invoked once per new
        //    // thread that will use the NanoLog system.
        NanoLog::preallocate();

        // Optional: Set the minimum LogLevel that log messages must have to be
        // persisted. Valid from least to greatest values are
        // DEBUG, NOTICE, WARNING, ERROR
        NanoLog::setLogLevel(NOTICE);
        NANO_LOG(NOTICE,",Message,Time(ns)");

    //1. register
    int64_t index_for_customer_use = -1;
    if(!g_shared_mem_ring_buffer.RegisterConsumer(customer_id, &index_for_customer_use )) {
        return; //error
    }
    //2. run
    size_t   total_fetched = 0;
    int64_t  my_index = index_for_customer_use ;
    bool is_first=true;
    char msg_buffer[1024];

    for(size_t i=0; i < SharedMemRingBuffer::READ_WRITE_COUNT   ; i++) {
        if( total_fetched >= SharedMemRingBuffer::READ_WRITE_COUNT ) {
            break;
        }
        int64_t returned_index = g_shared_mem_ring_buffer.WaitFor(customer_id, my_index);
        if(is_first) {
            is_first=false;
            g_elapsed.SetStartTime();
        }
        int writeIndex = -1;
        for(int64_t j = my_index; j <= returned_index; j++) {
            //batch job
            OneBufferData* pData = g_shared_mem_ring_buffer.GetData(j);
            int x = duration_cast<nanoseconds>(high_resolution_clock::now() - pData->tp).count();
            NANO_LOG(NOTICE,",Consumer %s %d,%d", pData->strVal ,pData->data, x);
            g_shared_mem_ring_buffer.CommitRead(0, j );
            total_fetched++;
            if(pingId > -1)
            {
                OneBufferData my_data;
                writeIndex = g_ping_shared_mem_ring_buffer.ClaimIndex(customer_id);
                my_data.data = -pData->data;
                my_data.tp = pData->tp;
                strcpy(my_data.strVal, pData->strVal);
                g_ping_shared_mem_ring_buffer.SetData( writeIndex, &my_data );
                g_ping_shared_mem_ring_buffer.Commit(0, writeIndex);
            }

        } //for

        my_index = returned_index + 1;
    } //for

    NANO_LOG(NOTICE,"Total fetched %d", total_fetched);

    long long nElapsedMicro = g_elapsed.SetEndTime(MICRO_SEC_RESOLUTION);
    snprintf(msg_buffer, sizeof(msg_buffer), "**** consumer test %lu count %lu -> "
            "elapsed :%lld (micro sec) TPS %lld",
        g_test_index , SharedMemRingBuffer::READ_WRITE_COUNT , nElapsedMicro,
        (long long) (10000L*1000000L)/nElapsedMicro );
    DEBUG_LOG(msg_buffer);
}

int map_thread_to_cpu(int core_id)
{
   int num_cores = sysconf(_SC_NPROCESSORS_ONLN);
   if (core_id < 0 || core_id >= num_cores) {
        return -1;
   }
   cpu_set_t cpuset;
   CPU_ZERO(&cpuset);
   CPU_SET(core_id, &cpuset);
   pthread_t current_thread = pthread_self();

   return pthread_setaffinity_np(current_thread, sizeof(cpu_set_t), &cpuset);
}

int main(int argc, char *argv[])
{
    int bufId = 0;
    std::string inputKey = "";
    int pingBufId = -1;
    int cpuIndex = -1;
    for( int i = 0; i < argc-1; i++)
    {
        std::string arg = argv[i];
        if( arg.compare("buf") == 0)
        {
            std::string bufNo = argv[i+1];
            bufId = std::stoi(bufNo);
            i++;
            continue;
        }
        else if( arg.compare("cpu") == 0 )
        {
            std::string cpuIndexString = argv[i+1];
            cpuIndex = std::stoi(cpuIndexString);
            i++;
            continue;
        }
        else if ( arg.compare("ping") == 0 )
        {
            std::string pingBufIdString = argv[i+1];
            pingBufId = std::stoi(pingBufIdString);
            i++;
            continue;
        }
    }
    if( cpuIndex >= 0)
    {
        map_thread_to_cpu(cpuIndex);
    }
    else
    {   map_thread_to_cpu(2);

    }
//    int customer_id = atoi(argv[1]);
    int customer_id = 0;
    if (bufId >= 0)
    {
        if(! g_shared_mem_ring_buffer.InitRingBuffer(123456+bufId, SharedMemRingBuffer::MAX_RBUFFER_CAPACITY) ) {
            //Error!
            return 1;
        }
    }
    if( pingBufId >= 0)
    {
        if(! g_ping_shared_mem_ring_buffer.InitRingBuffer(234567+pingBufId,SharedMemRingBuffer::MAX_RBUFFER_CAPACITY) ) {
            //Error!
            return 1;
        }
    }

    TestFunc(customer_id, bufId, pingBufId);
    return 0;
}
